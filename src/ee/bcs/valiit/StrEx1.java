package ee.bcs.valiit;

public class StrEx1 {

    public static void main (String[] args){
        String tallinnPopulation = "450 000";
        StringBuilder stringBuilder = new StringBuilder("Tallinnas elab").append(tallinnPopulation).append("inimest");
        System.out.println (stringBuilder);
    }
}
