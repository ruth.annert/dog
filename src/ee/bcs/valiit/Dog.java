package ee.bcs.valiit;

public class Dog extends LivingThing {

    public Dog() {} //vaikimisi konstruktor
    public Dog (String name){ //parameetritega konstruktor
        this.name = name;
        setType("Mammal");
    }
    private String name;

    public String getName(){
        return name;
    }

    public void setName (String name) {
    this.name = name;
    }


    public void bark() {
        System.out.println(getType()+" "+ name + " tegi Auh-Auh!," + Main. BIG_CONSTANT);
    }
}
