package ee.bcs.valiit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayListEx {

    public static void main (String [] args) {

        List<String> list = new ArrayList<>(Arrays.asList("Tallinn", "Tartu", "Pärnu", "Kärdla", "Võru"));
    /* Plan B
    list.add("Tallinn");
    list.add ("Tartu");
    list.add ("Pärnu");
    list.add ("Kärdla");
    list.add ("Võru");
    */
    list.add("Narva");
    list.set(2, "Narva");
    list.remove( "Võru");

        System.out.println(list.get(0));
        System.out.println(list.get(2));
        System.out.println(list.size()-1);

        for (String linn : list)
            System.out.println(linn);

        }
    }


