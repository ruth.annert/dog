package ee.bcs.valiit;

public class TrafficLightEx {
    public static void main (String [ ] args) {
        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("green")) {
                System.out.println("Driver can drive a car.");
            } else if (args[0].equalsIgnoreCase("yellow")) {
                System.out.println("Driver has to be ready to stop the car or start with driving.");
            } else if (args[0].equalsIgnoreCase("red")) {
                System.out.println("Driver has to stop car and wait for green light.");
            }
            else System.out.println ("Input error");

        }
    }
    }


