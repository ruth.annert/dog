package ee.bcs.valiit;

public class Main {

    public static final String BIG_CONSTANT = "Plahh";

    public static void main(String[] args) {

	    Dog koer1 = new Dog("Muki");
	    Dog koer2 = new Dog ();
	    koer2.setType("Bird");
	    koer2.setName("Muki");
	    koer1.setType("Mammal");
		koer1.bark();


		Dog koer2 = new Dog();
		koer2.setName("Muri");
		koer2.setType("Bird");
	    koer1.bark();
	    koer2.bark();
	    koer2.setName(koer1.getName());

    }
}
