package ee.bcs.valiit;

public class StringB1 {

        public static void main(String[] args) {
            String president1 = "Konstantin Päts";
            String president2 = "Lennart Meri";
            String president3 = "Arnold Rüütel";
            String president4 = "Toomas Hendrik Ilves";
            String president5 = "Kersti Kaljulaid";

            StringBuilder stringBuilder= new StringBuilder(president1)
                    .append(", ")
                    .append(president2).append(", ")
                    .append(president3).append(", ")
                    .append(president5).append(" on Eesti presidendid.");

            System.out.println(stringBuilder);
        }
        }
