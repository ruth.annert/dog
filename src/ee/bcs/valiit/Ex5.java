package ee.bcs.valiit;

public class Ex5 {
    public static String deriveGendre (String identificationNumber){
        if (identificationNumber.startsWith ("4") ||
        identificationNumber.startsWith ("6"))
        {
            return "F";
        }
         else if (identificationNumber.startsWith ("3") ||
                identificationNumber.startsWith ("5"))
        {
            return "M";
        }
        else return "unknown";
    }
}
