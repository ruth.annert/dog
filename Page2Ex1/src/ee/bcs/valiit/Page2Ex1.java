package ee.bcs.valiit;

public class Page2Ex1 {


        public static void main (String[] args){
            System.out.println("Hello, World!");
            System.out.println("\"Hello World\"!");
            System.out.println("Steven Hawking once said :\"Life would be tragic if it were'nt funny\".");


            String string1 = "\"See on teksti esimene pool";
            String string2 = " See on teksti teine pool\".";
            StringBuilder stringBuilder = new StringBuilder (string1).append(string2);
            System.out.println(stringBuilder);

            System.out.println("Elu on ilus.");
            System.out.println("Elu on \'ilus\'.");
            System.out.println("Elu on \"ilus\".");
            System.out.println("Kõige rohkem segadust tekitab “-märgi kasutamine sõne sees.");
            System.out.println("‘Kolm’ - kolm, ‘neli’ - neli, “viis” - viis.");
        }
}
